package com.devcamp.s50.task5840.cproductlist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CproductlistApplication {

	public static void main(String[] args) {
		SpringApplication.run(CproductlistApplication.class, args);
	}

}
