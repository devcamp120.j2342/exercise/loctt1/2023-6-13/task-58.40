package com.devcamp.s50.task5840.cproductlist.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "product")
public class CProduct {
     @Id
     @GeneratedValue(strategy = GenerationType.AUTO)
     private long id;

     @Column(name = "ma_san_pham")
     private String maSanPham;

     @Column(name = "price")
     private long price;

     @Column(name = "ngay_tao")
     private long ngayTao;

     @Column(name = "ngay_cap_nhat")
     private long ngayCapnhat;

     
     public CProduct() {
     }


     public CProduct(long id, String maSanPham, long price, long ngayTao, long ngayCapnhat) {
          this.id = id;
          this.maSanPham = maSanPham;
          this.price = price;
          this.ngayTao = ngayTao;
          this.ngayCapnhat = ngayCapnhat;
     }


     public long getId() {
          return id;
     }


     public void setId(long id) {
          this.id = id;
     }


     public String getMaSanPham() {
          return maSanPham;
     }


     public void setMaSanPham(String maSanPham) {
          this.maSanPham = maSanPham;
     }


     public long getPrice() {
          return price;
     }


     public void setPrice(long price) {
          this.price = price;
     }


     public long getNgayTao() {
          return ngayTao;
     }


     public void setNgayTao(long ngayTao) {
          this.ngayTao = ngayTao;
     }


     public long getNgayCapnhat() {
          return ngayCapnhat;
     }


     public void setNgayCapnhat(long ngayCapnhat) {
          this.ngayCapnhat = ngayCapnhat;
     }

     
}
