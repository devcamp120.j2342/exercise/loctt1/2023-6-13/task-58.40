package com.devcamp.s50.task5840.cproductlist.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.s50.task5840.cproductlist.models.CProduct;

public interface CProductRepository extends JpaRepository<CProduct, Long>{
     
}
